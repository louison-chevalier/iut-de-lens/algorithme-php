<?php require_once 'navigation.php';


//Message par default
$msg = "";


//si la personne à deja une session
if (isset($_SESSION['nbsession'])){
    $_SESSION['nbsession'] +=1;
}
//sinon
else{
    $_SESSION['nbsession'] = 1;
}


//Première connexion - première partie
if ($_SESSION['nbsession'] == 1){
    $_SESSION['nbalea'] = rand(0,1000);
    $_SESSION['nbtest'] = 0;
    $_SESSION['lesnombres'] = array_push();
}


// Si l'utilisateur envoie le formulaire
if(isset($_POST['btAjouter'])){
    $valeur = $_POST['valeur'];
    if($valeur < $_SESSION['nbalea']){
        $msg = "<div class=\"alert alert-danger\" role=\"alert\">".$valeur." est trop petit !</div>";
    }
    if ($valeur > $_SESSION['nbalea']){
        $msg = "<div class=\"alert alert-danger\" role=\"alert\">".$valeur." est trop grande !</div>";
    }

    if ($valeur == $_SESSION['nbalea']){
        $msg = "<div class=\"alert alert-success\" role=\"alert\">BRAVO ! Vous avez trouvé ".$_SESSION['nbalea']." en K essais</div>";
    }
}

?>


<div class="container text-center" >
    <div class="well"><h1 class="text-center">Game</h1></div>
    <p>Hey, j'ai une idée de jeu !</p>
    <p>Je viens de choisir un nombre entre 1 et 1000.</p>
    <p>Essayez de trouver lequel !</p>
    <p>Vous avez droit à 10 tentatives.</p>
    <br>


    <form  method="post"  action="jeu.php">
        <p>Vous devinez ?
            <input type="number" id="valeur" name="valeur" min="0" max="1000">
            <input  type="submit" id="btAjouter" name="btAjouter" value="Ajouter">
            <?php echo $msg;   ?>
        </p>
    </form>




</div>

</body>
</html>
