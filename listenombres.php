<?php require_once 'navigation.php';?>

        <div class="container">
            <div class="well">
                <h1 class="text-center">Liste Nombres</h1>
                <h2 class="text-center">Statistiques du fichier</h2>
            </div>
            <ul>
            <?php

            $ligne = 0;
            $somme = 0;
            $i=0;

            $fichiername= 'assets/listeNombre.txt';
            $fichier = file($fichiername);

            foreach ($fichier as $uneligne){
                $uneligne=(integer) $uneligne;

                while ($i<1){
                    $min = $uneligne;
                    $max = $uneligne;
                    $i = 2;
                }


                if ($uneligne > $max){
                    $max = $uneligne;
                }

                if ($uneligne < $min){
                    $min = $uneligne;
                }

                $somme= $somme+ $uneligne;

                $ligne++;

            }
            $moyenne = $somme/$ligne;

            echo "Fichier : ".$fichiername."<br>";
            echo "Nombre de ligne : ".$ligne."<br>";
            echo "Min : ".$min."<br>";
            echo "Max : ".$max."<br>";
            echo "Somme : ".$somme."<br>";
            echo "Moyenne : ".$moyenne."<br>";

            ?>

            </ul>
        </div>
    </body>
</html>
<body>