<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
    <title>Hello</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="CSS/bootstrap.css">

    <!-- jQuery library -->
    <script type="text/javascript"  src="JS/jquery.js"></script>

    <!-- Latest compiled JavaScript -->
    <script type="text/javascript"  src="JS/bootsrap.js"></script>

    <script type="text/javascript"  src="JS/custom.js"></script>

    <style>
        li, h3{
            margin-top : 20px;
        }
        td{
            padding: 2px;
        }
        #nb{
            padding : 11px;
        }
        .nav{
            text-align: center;
        }
    </style>
</head>
<body>
<div class="container nav" style="margin-bottom: 15px;">
    <ul class="nav nav-pills">
        <li><a href="index.php#TP0">TP 0 : INTRO</a></li>
        <li><a href="index.php#TP1">TP 1 : RAPPELS</a></li>
        <li><a href="index.php#EXO">Exercices</a></li>
        <li><a href="bulletin.php">Bulletin</a></li>
        <li><a href="listeliens.php">Liste Liens</a></li>
        <li><a href="listenombres.php">Liste Nombres</a></li>
        <li><a href="inactivite.php">Mise en forme des données de l'inactivité</a></li>
        <li><a href="boxoffice.php">Classement au Box Office</a></li>
        <li><a href="jeu.php">Jeu</a></li>
    </ul>
</div>