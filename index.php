<?php require_once 'navigation.php';?>
    <div class="container">
        <h2 id="TP0" class="text-center">TP 0 : INTRO</h2>
        <div class="well">
            <h3>EXO 1</h3>
            <ul>
                <li>Quelles sont les balises qui permettent de délimiter un script PHP <br> " < ? php  ? >"</li>
                <li>Le PHP peut il être intérprété par le client ? <br> Non c'est un language serveur</li>
                <li>Quel symbole délimite les instructions PHP ? <br></li>
                <li>Par quel caractère débute une variable ? <br> "$"</li>
                <li>Comment réalise t-on des commentaires ? <br>// ou /* */</li>
                <li>Quelle instruction permet d'afficher du contenu sur la page ? <br> echo </li>
            </ul>
        </div>
        <div class="well">
            <h3>EXO 2</h3>
            <p>Créer 3 variables, une entière, une chaîne de caractères et un booleéen. Affichez leur contenu</p>
            <?php
            $entiere=23;
            $chaine = "ahahah";
            $booleen = 1;
            echo  "Entière : ".$entiere."<br> Chaine : ".$chaine."<br> Booleen :".$booleen." ";
            ?>
        </div>

        <div class="well">
            <h3>EXO 3</h3>
            <p>Soient deux variables entières $a et $b. Donnez le code permettant d'échanger les données de ces deux variables.</p>
            <?php
                $a= "Je suis un a";
                $b= "Je suis un b";
                $tempo = "";
                echo "A = ".$a."  ";
                echo "B = ".$b." ";
                $tempo = $a;
                $a = $b;
                $b = $tempo;
                echo " -->";
                echo " A = ".$a."  ";
                echo "B = ".$b."";
            ?>
        </div>

        <div class="well">
            <h3>Exercice 4 : conditions</h3>
            <p>On considère la variable $age = rand(1,100) qui donne à la variable $age un nombre aléatoire entre 1 et 99. Affichez l'age de la personne et si elle est majeure ou pas. Rechargez la page, le résultat changera à chaque fois.</p>
            <?php
            $age = rand(1,100);
            echo "Mon age : ".$age."<br>";

            if ($age < 18 ){
                echo "Je suis mineur";
            }
            else{
                echo "Je suis majeur";
            }
            ?>
        </div>

        <div class="well">
            <h3>Exercice 5 : Boucle for</h3>
            <p>On considère la variable $v = rand(0,11) qui donne à une variable $v un nombre entre 0 et 10. Affichez la table de multiplication de cette variable.</p>
            <?php
            $v = rand(0,11);
            echo "Nombre =".$v." ";
            echo "
            <table class=\"table table-dark\">
                <thead>
                <tr>
                    <th scope=\"col\">Multiplication</th>
                    <th scope=\"col\"></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>0</td>
                    <td scope=\"row\">".$v."</td>
                 </tr>";
            for ($i=2; $i <=  10; $i++){
                $ahah = $v;
                $ahah = $ahah*$i;
                echo "
                    <tr>
                    <td>".$i."</td>
                    <td scope=\"row\">".$ahah."</td>
                 </tr>
                ";
            }
            ?>
            </tbody>
        </table>

        <h2 id="TP1" class="text-center">TP 1 : RAPPELS</h2>
            <h3>Exercice 1</h3>
            <p>On commence par des exercices très faciles pour se mettre en route<br>
                Ecrivez "Bonjour le Monde" en utilisant PHP et une variable<br>
                Ecrivez dans une page tous les nombres entre 1 et 50. Il faut donc utiliser une boucle for<br>
                Initialisez un nombre $i au hasard avec la commande : $i = rand(0,100). $i vaudra entre 0 et 100. Affichez $i et, en utilisant une instruction if, affichez si $i est plus petit que 50.<br>
                Ecrivez dans la même page tous les nombres de 1 à 500 en retournant à la ligne à chaque dizaine. Cela nécessite une boucle for et une boucle if<br>
                Affichez la tableau de multiplication de tous les nombres entre 0 et 10. On affichera un tableau de 121 cases (11x11).</p>
             <?php

                echo "<hr>";
                $ohoh = "Bonjour le Monde";
                echo $ohoh;
                echo "<hr>";
                for ($i=0; $i < 50; $i++ ){
                    echo $i;
                }
                echo "<hr>";
                $i = rand(0,100);
                if ($i > 50){
                    echo $i;
                }

                echo "<hr>";
                $o = 0;
                 for ($i=1; $i < 500; $i++ ){
                     $o++;
                     echo $i;
                     if ($o == 10){
                        echo "<br>";
                         $o = 0;
                     }
                 }
                 ?>




        </div>
        <div id="EXO" class="well">
            <h3>Exercice</h3>
            <h4>N°1</h4>
            <?php
            for ($ligne = 1; $ligne <= 200; $ligne++){
                echo '<p> Je dois faire des sauvegardes régulières de mes fichiers. </p>';
            }
            ?>
            <hr>
            <h4>N°2</h4>
            <?php
            $p = 1;
            for ($m = 0; $m <= 1000; $m++){
                if($p == 1){
                    echo "".$m." ";
                    $p = 0;
                }
                else{
                    $p = 1;
                }
            }

            if (isset($_GET['heures'])){
                $heures = $_GET['heures'];
                $minutes = $_GET['minutes'];
                $secondes = $_GET['secondes'];


                $heures2 = $heures*60*60;
                $minutes2 = $minutes*60;
                $secondes2= $secondes+$minutes2+$heures2;
            }

            ?>
            <hr>
            <h4>N°3</h4>
            <form action="/TEST">
                <input type="text" name="heures" required placeholder="Heures">
                <input type="text" name="minutes" required placeholder="Minutes">
                <input type="text" name="secondes" required placeholder="Secondes">
                <input type="submit" id="btconvert" class="btconvert" name="btconvert">
            </form>

            <?php
                if (isset($_GET['heures'])) {
                    echo "<p style='font-weight: bold'>Heures : " . $heures . " - Minutes : " . $minutes . " - Secondes : " . $secondes . " = ".$secondes2." secondes  total</p>";
                }
            ?>

            <hr>
            <h4>N°4</h4>
            <form action="/TEST">
                <input type="text" name="age" required placeholder="Age">
                <input type="submit" id="btconvert" class="btconvert" name="btconvert">
            </form>
            <?php
                if (isset($_GET['age'])){
                    $age = $_GET['age'];

                    if($age < 18){
                        echo "Vous êtes mineur";
                    }
                    else{
                        echo "Vou êtes majeur";
                    }
                }
            ?>

            <hr>
            <h4>N°5</h4>
            <form action="/TEST">
                <input type="text" name="nom" required placeholder="nom">
                <input type="text" name="lage" required placeholder="Age">
                <input type="submit" id="btconvert" class="btconvert" name="btconvert">
            </form>
            <?php
            if (isset($_GET['lage'])){
                $lage = $_GET['lage'];
                $nom = $_GET['nom'];

                switch ($lage) {
                     case 6:
                     case 7:
                        echo "Poussin";
                        break;
                    case 8:
                    case 9:
                        echo "Pupile";
                        break;
                    case 10:
                    case 11:
                        echo "Minime";
                        break;
                    default :
                        echo 'Cadet' ;
                        break;
                }
            }
            ?>

            <hr>
            <h4>N°6</h4>
            <form action="/TEST">
                <input type="text" name="taille" required placeholder="Taille de votre sapin">
                <input type="submit" id="btconvert" class="btconvert" name="btconvert">
            </form>
            <p>
            <?php
            if (isset($_GET['taille'])){
                $taille = $_GET['taille'];
                $ligne=1;
                for ($g=0; $g < $taille;$g++){
                    for ($a=0; $a < $ligne; $a++){
                        echo"*";
                    }

                    $ligne++;
                    echo "<br>";
                }
            }
            ?>
            </p>


          <?php  echo "<hr>";  $t = [1, 2 , 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17];?>

            <table class="table table-dark" id="nb">
            <thead>
            <tr>
                <?php
                for ($i=1; $i <18; $i++){
                    echo "<th> </th>";
                }
                ?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                for ($i=1; $i <18; $i++){}

                    echo "<tr> <td style='font-weight: bold;'> ".$i." </td>";

                    for ($p=1; $p <18; $p++){
                        $calcul =$p*$i;
                        echo "<td> ".$calcul." </td>";

                    echo "<tr>";
                }
                ?>
            </tr>
            </tbody></table>
        </div>

        <hr>
        <h4>N°7</h4>
         <form action="/TEST/impots.php">
             <input type="text" name="name" required placeholder="Votre nom">
             <input type="text" name="age" required placeholder="Votre age">
             <select id="photo3" name="sexe">
                 <optgroup label=" - SEXE - ">
                     <option value="homme">Homme</option>
                     <option value="femme">Femme</option>
                 </optgroup>
             </select>
         <input type="submit" id="btenvoyer" class="btenvoyer" name="btenvoyer">
         </form>

         <hr>
         <h4>N°8</h4>
             <form action="index.php">
                 <input type="text" name="lataille" required placeholder="Taille de votre sapin">
                 <input type="submit" id="btconvert" class="btconvert" name="btconvert">
             </form>
             <p class="text-center">
                 <?php
                     if (isset($_GET['lataille'])){
                         $taille = $_GET['lataille'];
                         $ligne=1;
                         for ($g=0; $g < $taille;$g++){
                             for ($a=0; $a < $ligne; $a++){
                                 echo"*";
                             }
                             $ligne++;
                             echo "<br>";
                         }
                     }
                 ?>
             </p>
    </body>
</html>

