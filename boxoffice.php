<?php require_once 'navigation.php';?>

<div class="container">
    <div class="well" style="margin-bottom: 50px;"><h1 class="text-center">Classement au Box Office</h1></div>
    <div>
        <?php
        $fichier = file('assets/boxoffice.txt');

        $laligne = 0;

        foreach ($fichier as $uneligne) {
            $lesmots = explode('	', $uneligne);

            $nbcolonnes = count($lesmots);

            $tableau2[$laligne] = array( $lesmots[0], $lesmots[2],$lesmots[3], $lesmots[4]);
            $tableau3[$laligne] = array( $lesmots[3], $lesmots[0], $lesmots[2], $lesmots[4]);
            $tableau4[$laligne] = array( $lesmots[4], $lesmots[3]);

            $tableau5[$laligne] = $lesmots[4];
            $laligne++;
        }

        $lesannees = array_unique($tableau5);


        $p =0;
        $tempoligne =0;
        foreach ($lesannees as $lannee){
            foreach ($tableau4 as $uneligne){
                if ($uneligne[0] == $lannee){
                    $tempoligne =  $tempoligne+$uneligne[1];
                }
            }
            $tableau6[$p] = array($lannee, $tempoligne);
            $p++;
            $tempoligne=0;
        }

        $tableau2 = new ArrayObject($tableau2);
        $tableau2->asort();

        arsort($tableau3);
        ?>
            <h2 class="text-center">Triés dans l'ordre alphabétique</h2>
            <br>
            <div class="row" style="height: 50vh; overflow-y: auto;">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Titre</th>
                        <th scope="col">Réalisateur</th>
                        <th scope="col">Entrées réalisées	</th>
                        <th scope="col">Année de production</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //Triés dans l'ordre alphabétique
                    foreach ($tableau2 as $uneligne){
                        echo " <tr><td>".$uneligne[0]."</td>";
                        echo " <td>".$uneligne[1]."</td>";
                        echo " <td>".$uneligne[2]."</td>";
                        echo " <td>".$uneligne[3]."</td></tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>


            <h2 class="text-center" style="margin-top: 100px;">Les 25 meilleurs au Box Office</h2>
            <br>
            <div class="row" style="height: 50vh; overflow-y: auto;">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Classement</th>
                        <th scope="col">Titre</th>
                        <th scope="col">Réalisateur</th>
                        <th scope="col">Entrées réalisées	</th>
                        <th scope="col">Année de production</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //Les 25 meilleurs au Box Office
                    $i = 1;

                    foreach ($tableau3 as $uneligne){
                        if($i<26){
                            echo  "<tr><td>".$i."</td>";
                            echo " <td>".$uneligne[1]."</td>";
                            echo " <td>".$uneligne[2]."</td>";
                            echo " <td>".$uneligne[0]."</td>";
                            echo " <td>".$uneligne[3]."</td></tr>";
                            $i++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>


        <h2 class="text-center" style="margin-top: 100px;">Nombre d'entrées cumulées par année</h2>
        <br>
        <div class="row" style="height: 50vh; overflow-y: auto; margin-bottom: 100px;">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Année</th>
                    <th scope="col">Entrées réalisées</th>
                </tr>
                </thead>
                <tbody>
                <?php
                //Nombre d'entrées cumulées par année
                $i = 1;

                foreach ($tableau6 as $uneligne){
                        echo  "<tr><td>".$uneligne[0]."</td>";
                        echo " <td>".$uneligne[1]."</td></tr>";
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
